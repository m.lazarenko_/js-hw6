// Theory

// 1. Екранування потрібно для виводу символів або пошуку символів, наприклад ми хочемо вивести строку та в строці є одинарні лапки, щоб їх вивести, потрібно екранувати їх.

// 2. Arrow function, function declaration, function expression

// 3. Hoisting - механізм в js, в якому змінні та функції здвигаються вверх своєї видимості перед тим, як код буде компілюватися.




function createNewUser(firstName, lastName) {
    let newUser = {
        name : firstName,
        surname : lastName,
        birthday: prompt("Enter your birth date", "28.09.2003"),
        getLogin() {
            return this.name.slice(0, 1).toLowerCase() + this.surname.toLowerCase();
        },   
        getPassword() {
            return this.name.slice(0, 1) + this.surname.toLowerCase() + this.birthday.slice(6,10);
        },
        getAge() {
            let now = new Date();
            let bd = this.birthday.split("."); 
            bd.reverse();
            let bd1 = new Date(bd.join(",")); // Дата др 
            

            let age = (now.getTime() - bd1) / (24 * 3600 * 365.25 * 1000) | 0;;
            
            
            return age;
        }
    }
    return newUser
}

let y = createNewUser("Maksym", "Lazarenko","28.09.2003")

console.log(y)
console.log(y.getAge())
console.log(y.getPassword())

